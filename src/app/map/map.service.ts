import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MapService {
  coordinates = new google.maps.LatLng(-0.7893, 113.9213);
  marker = new google.maps.Marker({ position: this.coordinates });

  map: google.maps.Map;
  place: google.maps.places.PlaceResult;

  constructor(private ngZone: NgZone) { }

  setMap(mapNativeElement) {
    const options = {
      center: this.coordinates,
      zoom: 4,
      // mapTypeControl: false,
      // panControl: false,
      // zoomControl: false,
      // streetViewControl: false,
    };
    this.map = new google.maps.Map(mapNativeElement, options);
    this.marker.setMap(this.map);
    this.setPlace(this.place);
  }

  setPlace(place: google.maps.places.PlaceResult) {
    this.place = place;
    if (!this.map || !place || !place.geometry) return;

    if (place.geometry.viewport) {
      this.map.fitBounds(place.geometry.viewport);
    } else {
      this.map.setCenter(place.geometry.location);
      this.map.setZoom(17);
    }

    // @ts-ignore
    this.marker.setPlace({
      placeId: place.place_id,
      location: place.geometry.location
    });
    this.marker.setVisible(true);
    console.log('setPlace', place);
  }

  autocomplete$(input: HTMLInputElement, options: google.maps.places.AutocompleteOptions = {}) {
    return new Observable<google.maps.places.PlaceResult>(subsriber => {
      var ac = new google.maps.places.Autocomplete(input, options);
      ac.bindTo('bounds', this.map);
      ac.setFields(['place_id', 'name', 'geometry', 'icon']);
      ac.addListener('place_changed', () => this.ngZone.run(() => subsriber.next(ac.getPlace())));
    });
  }
}
