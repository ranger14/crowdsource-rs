import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, combineLatest, ReplaySubject } from 'rxjs';
import { map, switchMap, distinctUntilChanged, shareReplay, startWith, tap, take, filter } from 'rxjs/operators';

import { PlaceData, toPlaceData, KpData, JENIS_LAYANAN_RS, STATUS_KEPEMILIKAN_RS, ITEM_STATUS, validateBulkEdit } from '../rs/rs.component';
import { UserService } from '../user.service';
import { UtilService } from '../util.service';

function bestMatch(name: string, children: [string, string][]): string {
  name = (name || '').toUpperCase();
  if (name.startsWith('KABUPATEN ')) name = name.substring(10);
  if (name.startsWith('KEC. ')) name = name.substring(5);
  const cnt: any = {};
  for (let i = 0; i < name.length; i++) {
    cnt[name[i]] = (cnt[name[i]] ?? 0) + 1;
  }
  let mostCommon = -1;
  let selectedId = '';
  for (const c of children) {
    let [id, cname] = c;
    cname = cname.toUpperCase();
    const ccnt: any = {};
    for (let i = 0; i < cname.length; i++) {
      ccnt[cname[i]] = (ccnt[cname[i]] ?? 0) + 1;
    }
    let common = 0;
    for (const [k, v] of Object.entries<number>(ccnt)) {
      const c = cnt[k] ?? 0;
      common += Math.min(c, v);
    }
    if (common > mostCommon) {
      mostCommon = common;
      selectedId = id;
    }
  }
  return selectedId;
}

@Component({
  selector: 'app-rs-update',
  templateUrl: './rs-update.component.html',
  styleUrls: ['rs-update.component.css', '../rs-detail/rs-detail.component.css']
})
export class RsUpdateComponent {
  JENIS_LAYANAN_RS = JENIS_LAYANAN_RS;
  JENIS_LAYANAN_RS_KEYS = Object.keys(JENIS_LAYANAN_RS);
  STATUS_KEPEMILIKAN_RS = STATUS_KEPEMILIKAN_RS;
  STATUS_KEPEMILIKAN_RS_KEYS = Object.keys(STATUS_KEPEMILIKAN_RS);

  placeData$: Observable<PlaceData>;
  updatingRsInfo: boolean = false;
  rsForm: FormGroup;

  // To change default selection.
  propinsiTrigger = new ReplaySubject<string>(1);
  kabupatenTrigger = new ReplaySubject<string>(1);
  kecamatanTrigger = new ReplaySubject<string>(1);
  kelurahanTrigger = new ReplaySubject<string>(1);

  // KawalPemilu lokasi data
  nasionalKpData$: Observable<[string, string][]>;
  propinsiKpData$: Observable<[string, string][]>;
  kabupatenKpData$: Observable<[string, string][]>;
  kecamatanKpData$: Observable<[string, string][]>;

  constructor(
    public readonly userService: UserService,
    public readonly location: Location,
    public readonly utilService: UtilService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly firestore: AngularFirestore,
    readonly formBuilder: FormBuilder,
  ) {
    this.rsForm = this.formBuilder.group({
      isRujukan: [null],
      jenisLayanan: [null],
      statusKepemilikan: [null],
      icu: [null, [Validators.pattern(/^[0-9]{0,6}$/)]],
      isolasi: [null, [Validators.pattern(/^[0-9]{0,6}$/)]],
      availableVentilator: [null, [Validators.pattern(/^[0-9]{0,6}$/)]],
      availableMechanicalVentilator: [null, [Validators.pattern(/^[0-9]{0,6}$/)]],
      availableEcmo: [null, [Validators.pattern(/^[0-9]{0,6}$/)]],
      jumlahNakes: [null, [Validators.pattern(/^[0-9]{0,6}$/)]],
      contactName: [null],
      contactEmail: [null, Validators.email],
      contact: [null, [Validators.pattern(/^62[0-9]{7,15}$/)]],
    });

    const placeDataRaw$ =
      this.route.paramMap.pipe(
        map((params: ParamMap) => params.get('id')),
        filter<string>(Boolean),
        distinctUntilChanged(),
        switchMap((placeId: string) =>
          this.firestore.collection('rs')
            .doc<PlaceData>(placeId)
            .valueChanges()));

    this.placeData$ =
      combineLatest(placeDataRaw$, this.userService.user$.pipe(startWith(null)))
        .pipe(
          map(([placeDataRaw, user]) => toPlaceData(placeDataRaw, user)),
          tap(placeData => {
            this.rsForm.get('isRujukan').setValue(placeData.latest.isRujukan?.value);
            this.rsForm.get('jenisLayanan').setValue(placeData.latest.jenisLayanan?.value);
            this.rsForm.get('statusKepemilikan').setValue(placeData.latest.statusKepemilikan?.value);
            this.rsForm.get('icu').setValue(placeData.latest.icu?.value);
            this.rsForm.get('isolasi').setValue(placeData.latest.isolasi?.value);
            this.rsForm.get('availableVentilator').setValue(placeData.latest.availableVentilator?.value);
            this.rsForm.get('availableMechanicalVentilator').setValue(placeData.latest.availableMechanicalVentilator?.value);
            this.rsForm.get('availableEcmo').setValue(placeData.latest.availableEcmo?.value);
            this.rsForm.get('jumlahNakes').setValue(placeData.latest.jumlahNakes?.value);
            this.rsForm.get('contactName').setValue(placeData.latest.contactName?.value);
            this.rsForm.get('contactEmail').setValue(placeData.latest.contactEmail?.value);
            this.rsForm.get('contact').setValue(placeData.latest.contact?.value);
            this.rsForm.markAsPristine();
          }),
          shareReplay(1)
        );

    this.nasionalKpData$ =
      this.placeData$.pipe(
        switchMap(async placeData => {
          const nasionalKpData = Object.entries((await this.userService.get<KpData>('h/0')).children);
          if (placeData.kpData && nasionalKpData.find(c => +c[0] === placeData.kpData.parentIds[1])) {
            this.propinsiTrigger.next(String(placeData.kpData.parentIds[1]));
          } else {
            const propinsi = placeData.place.address_components.filter(
              addr => addr.types.indexOf('administrative_area_level_1') > -1)[0]?.['short_name'];
            this.propinsiTrigger.next(bestMatch(propinsi, nasionalKpData));
          }
          return nasionalKpData;
        }),
        shareReplay(1));

    this.propinsiKpData$ =
      combineLatest(this.placeData$, this.propinsiTrigger).pipe(
        switchMap(async ([placeData, propinsi]) => {
          const propinsiKpData = Object.entries(
            (await this.userService.get<KpData>(`h/${propinsi}`)).children);
          if (placeData.kpData && propinsiKpData.find(c => +c[0] === placeData.kpData.parentIds[2])) {
            this.kabupatenTrigger.next(String(placeData.kpData.parentIds[2]));
          } else {
            const kabupaten = placeData.place.address_components.filter(
              addr => addr.types.indexOf('administrative_area_level_2') > -1)[0]?.['short_name'];
            this.kabupatenTrigger.next(bestMatch(kabupaten, propinsiKpData));
          }
          return propinsiKpData;
        }),
        shareReplay(1)
      );

    this.kabupatenKpData$ =
      combineLatest(this.placeData$, this.kabupatenTrigger).pipe(
        switchMap(async ([placeData, kabupaten]) => {
          const kabupatenKpData = Object.entries(
            (await this.userService.get<KpData>(`h/${kabupaten}`)).children);
          if (placeData.kpData && kabupatenKpData.find(c => +c[0] === placeData.kpData.parentIds[3])) {
            this.kecamatanTrigger.next(String(placeData.kpData.parentIds[3]));
          } else {
            const kecamatan = placeData.place.address_components.filter(
              addr => addr.types.indexOf('administrative_area_level_3') > -1)[0]?.['short_name'];
            this.kecamatanTrigger.next(bestMatch(kecamatan, kabupatenKpData));
          }
          return kabupatenKpData;
        }),
        shareReplay(1));

    this.kecamatanKpData$ =
      combineLatest(this.placeData$, this.kecamatanTrigger).pipe(
        switchMap(async ([placeData, kecamatan]) => {
          const kecamatanKpData = Object.entries(
            (await this.userService.get<KpData>(`h/${kecamatan}`)).children);
          if (placeData.kpData && kecamatanKpData.find(c => +c[0] === placeData.kpData.id)) {
            this.kelurahanTrigger.next(String(placeData.kpData.id));
          } else {
            const kelurahan = placeData.place.address_components.filter(
              addr => addr.types.indexOf('administrative_area_level_4') > -1)[0]?.['short_name'];
            this.kelurahanTrigger.next(bestMatch(kelurahan, kecamatanKpData));
          }
          return kecamatanKpData;
        }),
        shareReplay(1));
  }

  async saveRsInfo(placeData: PlaceData) {
    const kpId = +(await this.kelurahanTrigger.pipe(take(1)).toPromise());
    const value = {};
    if (placeData?.kpData?.id !== kpId) value['kpId'] = kpId;
    for (let [k, v] of Object.entries(this.rsForm.value)) {
      if (v === null || v === undefined) continue;
      if (k === 'isRujukan') v = v ? 1 : 0;
      value[k] = { value: v, status: ITEM_STATUS.unset };
    }
    console.log('save', kpId, placeData?.kpData?.id, value);
    const error = validateBulkEdit(value);
    if (error) {
      this.utilService.sendMessage({ message: error, type: 'error' });
      return;
    }

    this.updatingRsInfo = true;
    try {
      const res: any = await this.userService.post(`bulk_edit/${placeData.place.place_id}`, value);
      if (res.error) this.utilService.sendMessage({ message: res.error, type: 'error' });
      else this.utilService.sendMessage({ message: 'Informasi rumah sakit berhasil disimpan', type: 'success' });
    } catch (e) {
      this.utilService.sendMessage({ message: e.message, type: 'error' });
    }
    this.updatingRsInfo = false;
    this.router.navigate(['/rs', placeData.place.place_id]);
  }
}
