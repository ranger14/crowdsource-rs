﻿import { NgModule } from '@angular/core';

import {
  FacebookShareComponent,
  TwitterShareComponent,
  WhatsAppShareComponent,
  GooglePlusShareComponent,
  LinkedInShareComponent
} from './share.component';


@NgModule({
  declarations: [
    FacebookShareComponent,
    TwitterShareComponent,
    WhatsAppShareComponent,
    GooglePlusShareComponent,
    LinkedInShareComponent
  ],
  exports: [
    FacebookShareComponent,
    TwitterShareComponent,
    WhatsAppShareComponent,
    GooglePlusShareComponent,
    LinkedInShareComponent
  ]
})
export class SocialMediaButtons { }
