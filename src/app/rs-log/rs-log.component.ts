import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { PlaceData, USER_ROLE, Relawan, Attributes  } from '../rs/rs.component';
import { UserService } from '../user.service';
import { Observable } from 'rxjs';
import { UtilService } from '../util.service';

export interface Revision {
  key: string;
  label: string;
  value: string;
  prev: string;
  uid: string;
  ts: Date;
  updatedAgo: string;
  // user$: Observable<Relawan>;
}
interface GroupByUser {
  user$: Observable<Relawan>;
  rev:Revision[]
}

interface GroupByDate {
  updatedAt: string;
  userChanges:GroupByUser[]
}



@Component({
  selector: 'app-rs-log',
  templateUrl: './rs-log.component.html',
  styleUrls: ['./rs-log.component.css']
})
export class RsLogComponent implements OnInit, OnChanges {
  @Input() placeData: PlaceData;

  USER_ROLE = USER_ROLE;
  changes: Revision[] = [];
  changesByDate:GroupByDate[] = [];

  groupKey = {};
  shortLabel = {};
  unitLabel = {};

  constructor(private userService: UserService, private utilService:UtilService) {
    for (let a of Attributes.suspect) {
      this.groupKey[a.key] = 'suspects';
      this.shortLabel[a.key] = a.name || a.shortLabel;
    }
    for (let a of Attributes.logistic) {
      this.groupKey[a.key] = 'logistics';
      this.shortLabel[a.key] = a.name || a.shortLabel;
      this.unitLabel[a.key] = a.unitLabel;
    }
    for (let a of Attributes.bed) {
      this.groupKey[a.key] = 'beds';
      this.shortLabel[a.key] = a.name || a.shortLabel;
    }
    for (let a of Attributes.availibility) {
      this.groupKey[a.key] = 'availabilities';
      this.shortLabel[a.key] = a.name || a.shortLabel;
      this.unitLabel[a.key] = a.unitLabel;
    }
    for (let a of Attributes.info) {
      this.groupKey[a.key] = 'info';
      this.shortLabel[a.key] = a.name || a.shortLabel;
    }
  }

  ngOnInit() {
    this.ngOnChanges();
  }

  ngOnChanges() {
    this.changesByDate = [];

    let changes = [];
    for (const [key, arr] of Object.entries(this.placeData.history ?? {})) {
      let prev;
      for (const h of arr) {
        changes.push({
          key,
          label: this.shortLabel[key],
          uid: h.uid,
          ts: new Date(h.ts),
          updatedAgo: this.utilService.timeAgo(h.ts),
          value: h.value,
          prev,
          user$: null,
        });
        prev = h.value;
      }
    }
    changes.sort((a, b) => b.ts.getTime() - a.ts.getTime()).splice(15);

    // for (const c of changes) {
    //   c.user$ = this.userService.getUser$(c.uid);
    // }

    const groupByDate = this.groupBy(changes, 'updatedAgo');
    for(const updatedAgo of Object.keys(groupByDate)){
      const gbd:GroupByDate = {
        updatedAt: updatedAgo,
        userChanges:[]
      }
      const groupByUser = this.groupBy(groupByDate[updatedAgo], 'uid');
      for (const uid of Object.keys(groupByUser)) {
        gbd.userChanges.push({
          user$: this.userService.getUser$(uid),
          rev: groupByUser[uid]
        });
      }
      this.changesByDate.push(gbd)
    }
    // console.log(groupByDate)
    // groupByDate.forEach((val, key)=>{
    //   console.log(key)
    // });
    //
    // console.log(this.changes)
  }

  private groupBy(xs, key) {
    return xs.reduce(function(rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  }
}
