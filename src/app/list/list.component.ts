import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { UtilService } from '../util.service';
import { ITEM_STATUS, RsData } from '../rs/rs.component';
import { Observable, combineLatest, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  // Overrides the rsData if set.
  @Input() places$: Observable<RsData[]>;

  ITEM_STATUS = ITEM_STATUS;
  rsData$: Observable<RsData[]>;
  filterText$ = new BehaviorSubject('');
  filterText = '';
  sortBy$ = new BehaviorSubject('date');

  constructor(
    public utilService: UtilService,
    public userService: UserService,
  ) {
  }

  ngOnInit() {
    const rsData$ = this.places$ ? this.places$ : this.utilService.allPlaceData$;
    this.rsData$ = combineLatest(rsData$, this.filterText$, this.sortBy$)
      .pipe(map(([rsData, filterText, sortBy]) => {
        const f = filterText.toLowerCase();
        const res = !f ? rsData : rsData.filter(rs => rs.placeData.place.name.toLowerCase().indexOf(f) !== -1 ||
          rs.placeData.place.formatted_address.toLowerCase().indexOf(f) !== -1);
        return sortBy === 'date'
          ? res.sort((a, b) => b.lastUpdateTs - a.lastUpdateTs)
          : res.sort((a, b) => a.distanceToUser - b.distanceToUser);
      }));
  }
  getPropinsi(address_components) {
    return address_components
      .filter(addr => addr.types.indexOf('administrative_area_level_1') > -1)[0]?.['short_name'];
  }
}
