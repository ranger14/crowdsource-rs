import { Component, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { map, filter, switchMap, distinctUntilChanged, shareReplay, startWith } from 'rxjs/operators';
import { PlaceData, toPlaceData, Attributes, AttributeDetail, ITEM_STATUS } from '../rs/rs.component';
import { SingleNumberCard } from './single-number.component';
import { UtilService } from '../util.service';
import { UserService } from '../user.service';
import { Location } from '@angular/common';

interface Details {
  placeData: PlaceData;
  shortSupply: AttributeDetail[];
  outStock: AttributeDetail[];
  unfilledLogistic: AttributeDetail[];

  // Okupansi ruang ICU
  okupansiICU: number;

  // Okupansi ruang Isolasi
  okupansiIsolasi: number;

  // Jumlah semua kebutuhan di RS ini (termasuk yang mendesak)
  totalKebutuhan: number;

  // Jumlah kebutuhan yang mendesak di RS ini.
  totalMendesak: number;
}

@Component({
  selector: 'app-rs-detail',
  templateUrl: './rs-detail.component.html',
  styleUrls: ['./rs-detail.component.css']
})
export class RsDetailComponent {
  @ViewChild(SingleNumberCard) singleNumberCard: SingleNumberCard;

  details$: Observable<Details>;
  attributes = Attributes;
  updatingContact = false;

  constructor(
    private route: ActivatedRoute,
    private firestore: AngularFirestore,
    public utilService: UtilService,
    public userService: UserService,
    public location: Location,
  ) {
    const placeDataRaw$ = this.route.paramMap.pipe(
      map((params: ParamMap) => params.get('id')),
      filter<string>(Boolean),
      distinctUntilChanged(),
      switchMap((placeId: string) =>
        this.firestore.collection('rs')
          .doc<PlaceData>(placeId)
          .valueChanges())
    );

    this.details$ =
      combineLatest(placeDataRaw$, this.userService.user$.pipe(startWith(null))).pipe(
        map(([placeDataRaw, user]) => toPlaceData(placeDataRaw, user)),
        map((placeData: PlaceData) => {
          const details: Details = {
            placeData,
            shortSupply: [],
            outStock: [],
            unfilledLogistic: [],
            okupansiICU: 0,
            okupansiIsolasi: 0,
            totalKebutuhan: 0,
            totalMendesak: 0,
          }
          for (const logistic of Attributes.logistic) {
            if (placeData.latest[logistic.key] && placeData.latest[logistic.key].value > 0) {
              const latest = placeData.latest[logistic.key];
              const value = latest.value;
              if (latest.hasOwnProperty('status')) {
                if (latest.status === ITEM_STATUS.short) details.shortSupply.push(logistic);
                if (latest.status === ITEM_STATUS.out) {
                  details.outStock.push(logistic);
                  details.totalMendesak += value;
                }
              } else {
                // unset status
                details.shortSupply.push(logistic);
              }
              details.totalKebutuhan += value;
            } else {
              details.unfilledLogistic.push(logistic);
            }
          }

          details.okupansiICU = placeData.latest?.usingIcu?.value > 0 ?
            (placeData.latest.usingIcu.value / placeData.latest.icu.value) * 100 : 0;

          details.okupansiIsolasi = placeData.latest?.usingIsolation?.value > 0 ?
            (placeData.latest.usingIsolation.value / placeData.latest.isolasi.value) * 100 : 0;

          // Ordering by logistic name
          const lexSortFn = (a, b) => (a.name > b.name) ? 1 : -1;
          details.shortSupply.sort(lexSortFn);
          details.outStock.sort(lexSortFn);
          details.unfilledLogistic.sort(lexSortFn);
          return details;
        }),
        shareReplay(1)
      );
  }

  async saveKondisi(placeId: string, value: string) {
    const res: any = await this.userService.post(`edit/${placeId}/kondisi`, { value: +value });
    if (res.error) {
      alert(res.error);
    }
  }

  openDialog(placeData: PlaceData, l: AttributeDetail) {
    this.singleNumberCard.openDialog({
      title: l.formLabel,
      placeData,
      key: l.key,
      unitLabel: l.unitLabel,
      hold: true,
      hasStatus: true,
    });
  }

  async editContact(placeData: PlaceData) {
    const contact = prompt(
      'Masukkan nomor WA orang yang bisa dihubungi untuk permintaan alat-alat di RS/Puskesmas/Faskes ini:',
      `${placeData.latest.contact?.value}`);
    if (contact && contact !== placeData.latest.contact?.value) {
      this.updatingContact = true;
      try {
        const res: any = await this.userService.post(`edit/${placeData.place.place_id}/contact`, { value: contact });
        if (res.error) {
          alert(res.error);
        }
      } catch (e) {
        alert(e.message);
        console.error(e);
      }
      this.updatingContact = false;
    }
  }

  contactFaskes(contact: string) {
    if (contact.startsWith('0')) {
      contact = `62${contact.substring(1)}`;
    }
    document.location.href = `https://wa.me/${contact}`;
  }
}
