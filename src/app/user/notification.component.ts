import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { UtilService } from '../util.service';

@Component({
  selector: 'app-user-notification',
  templateUrl: 'notification.component.html'
})
export class UserNotificationComponent {

  constructor(
    public userService: UserService,
    public utilService: UtilService,
  ) {
  }
}
