import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { UtilService } from '../util.service';

@Component({
  selector: 'app-user',
  template: `
    <div class="menus">
      <a routerLink="profile" routerLinkActive="active">Perubahan</a>
      <ng-container *ngIf="utilService.notificationCount$ | async as count">
        <a routerLink="notification" routerLinkActive="active"
          [matBadge]="count" matBadgePosition="above after" [matBadgeHidden]="!count">Notification</a>
      </ng-container>
      <a *ngIf="userService.isModerator"
         routerLink="admin" routerLinkActive="active">Admin</a>
    </div>
    <router-outlet></router-outlet>`,
  styleUrls: ['user.component.css'],
})
export class UserComponent {
  constructor(public userService: UserService,
    public utilService: UtilService
  ) { }
}
