import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { UserService } from '../user.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-link',
  template: `
    <a *ngIf="user$ | async as user"
      routerLinkActive="active"
      [routerLink]="['/user', 'admin', 'u', uid]">
      {{ user.pre }}<b>{{ user.mid }}</b>{{ user.post }}
    </a>`,
  styles: [`
    a.active {
      font-weight: bold;
      text-decoration: underline;
    }
  `],
})
export class UserLinkComponent implements OnInit, OnChanges {
  @Input() uid = '';
  @Input() highlight = '';

  user$: Observable<{ pre: string, mid: string, post: string }>;

  constructor(private userService: UserService) { }

  ngOnChanges() {
    if (!this.uid) return;
    this.user$ = this.userService.getUser$(this.uid).pipe(map(user => {
      const name = user.displayName;
      const i = user.nameLowerCase.indexOf(this.highlight);
      const pre = name.substring(0, i);
      const mid = name.substring(i, i + this.highlight.length);
      const post = name.substring(i + this.highlight.length);
      return { pre, mid, post };
    }));
  }

  ngOnInit() {
    this.ngOnChanges();
  }
}
