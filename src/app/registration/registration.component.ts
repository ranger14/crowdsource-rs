import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { USER_ROLE, RegistrationInfo, ImageMetadata } from '../rs/rs.component';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { take, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'firebase/storage';
import * as piexif from 'piexifjs';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  USER_ROLE = USER_ROLE;
  form: FormGroup;
  percentage$: Observable<number>;
  status = 'new';
  uploading = false;
  showForm = true;

  constructor(
    public userService: UserService,
    private fb: FormBuilder,
    private storage: AngularFireStorage) {

    this.userService.user$.pipe(take(1)).toPromise().then(user => {
      const reg = user.registration || {} as RegistrationInfo;

      if (userService.isRegistrationInfoCompleted) {
        this.showForm = false;
        this.status = 'update';
      }

      this.form = this.fb.group({
        profesi: [reg.profesi ?? '', Validators.maxLength(64)],
        profesiLainnya: [reg.profesiLainnya ?? '', Validators.maxLength(64)],
        instansi: [reg.instansi ?? '', Validators.maxLength(64)],
        regEmail: [reg.regEmail ?? user.email ?? '', Validators.maxLength(64)],
        noHp: [reg.noHp ?? user.phoneNumber ?? '', Validators.maxLength(64)],
        fotoIdCard: [reg.fotoIdCard ?? '', Validators.maxLength(1024)],
        fotoIdCardMeta: [{}],
        deskripsi: [reg.deskripsi ?? '', Validators.maxLength(1024)],
      });
    });
  }

  async updateProfile() {
    const v = { ...this.form.value };
    this.status = 'submitting';
    console.log('submit', v);
    try {
      const result: any = await this.userService.post('update_profile', v);
      if (!result.error) {
        this.status = 'done';
        this.form.markAsPristine();
        return;
      }
      alert(result.error);
      console.error(result);
    } catch (e) {
      alert(e.message);
      console.error(e);
    }
    this.status = 'dirty';
  }

  change() {
    this.form.get('profesi').setValue('Lainnya');
  }

  async onFileSelected(event) {
    this.uploading = true;
    let file = event.target.files[0];
    console.log('file selected', file);

    try {
      const m = await this.getImageMetadata(file);
      file = m.file;
      this.form.get('fotoIdCardMeta').setValue(m.metadata);
    } catch (e) {
      console.error('Unable to extract metadata', e);
    }

    const randomId = Math.random().toString(36).substring(2);
    const filePath = `id_card/${this.userService.userid}/${randomId}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.percentage$ = task.percentageChanges();
    task.snapshotChanges().pipe(
      finalize(() => fileRef.getDownloadURL().subscribe(
        url => {
          this.form.get('fotoIdCard').setValue(url);
          this.form.markAsDirty();
          this.uploading = false;
        }))
    ).subscribe();
  }

  private async getImageMetadata(file: File) {
    const metadata = { s: file.size, l: file.lastModified } as ImageMetadata;
    let imgURL: string | ArrayBuffer = await this.readAsDataUrl(file);
    const exifObj = this.populateMetadata(imgURL, metadata);
    if (file.size > 800 * 1024) {
      imgURL = await this.compress(imgURL, 2048);
      if (exifObj) {
        try {
          // https://piexifjs.readthedocs.io/en/2.0/sample.html#insert-exif-into-jpeg
          imgURL = piexif.insert(piexif.dump(exifObj), imgURL);
        } catch (e) {
          console.error(e);
        }
      }
      file = this.dataURLtoBlob(imgURL) as File;
      metadata.z = file.size;
    }
    return { file, metadata, imgURL };
  }

  private readAsDataUrl(file: File): Promise<string | ArrayBuffer> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = reject;
    });
  }

  private populateMetadata(imgURL, m) {
    try {
      const exifObj = piexif.load(imgURL as string);
      const z = exifObj['0th'];
      if (z) {
        m.w = z[piexif.TagValues.ImageIFD.ImageWidth];
        m.h = z[piexif.TagValues.ImageIFD.ImageLength];
        m.m = [
          z[piexif.TagValues.ImageIFD.Make] as string,
          z[piexif.TagValues.ImageIFD.Model] as string
        ];
        m.o = z[piexif.TagValues.ImageIFD.Orientation];
      }
      const g = exifObj['GPS'];
      if (g) {
        m.y = this.convertDms(
          g[piexif.TagValues.GPSIFD.GPSLatitude],
          g[piexif.TagValues.GPSIFD.GPSLatitudeRef]
        );
        m.x = this.convertDms(
          g[piexif.TagValues.GPSIFD.GPSLongitude],
          g[piexif.TagValues.GPSIFD.GPSLongitudeRef]
        );
      }
      return exifObj;
    } catch (e) {
      return null;
    }
  }

  private async compress(dataUrl, maxDimension): Promise<string> {
    const img = await this.getImage(dataUrl);
    let width = img.width;
    let height = img.height;
    const scale = Math.min(1, maxDimension / width, maxDimension / height);
    if (scale < 1) {
      width *= scale;
      height *= scale;
    }
    const elem = document.createElement('canvas'); // Use Angular's Renderer2 method
    elem.width = width;
    elem.height = height;
    const ctx = elem.getContext('2d');
    ctx.drawImage(img, 0, 0, width, height);
    return ctx.canvas.toDataURL('image/jpeg');
  }

  private getImage(dataUrl): Promise<HTMLImageElement> {
    const img = new Image();
    return new Promise(resolve => {
      img.src = dataUrl;
      img.onload = () => resolve(img);
    });
  }

  /**
   * From: https://gist.github.com/wuchengwei/b7e1820d39445f431aeaa9c786753d8e
   */
  private dataURLtoBlob(dataurl) {
    const arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      u8arr = new Uint8Array(bstr.length);

    for (let n = bstr.length; n--;) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], { type: mime });
  }

  private convertDms(dms, direction) {
    if (!dms || !direction || dms.length < 3) {
      return null;
    }
    const degs = dms[0][0] / dms[0][1];
    const mins = dms[1][0] / dms[1][1];
    const secs = dms[2][0] / dms[2][1];
    return this.convertDMSToDD(degs, mins, secs, direction);
  }

  private convertDMSToDD(degrees, minutes, seconds, direction) {
    let dd = degrees + minutes / 60.0 + seconds / (60.0 * 60);
    if (direction === 'S' || direction === 'W') {
      dd = dd * -1;
    } // Don't do anything for N or E
    return dd;
  }

  toggleForm() {
    if (this.showForm === false) this.showForm = true;
    else this.showForm = false;
  }
}
