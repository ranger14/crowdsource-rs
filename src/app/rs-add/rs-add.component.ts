import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MapService } from '../map/map.service';
import { UserService } from '../user.service';
import { Observable, Subject, merge } from 'rxjs';
import { map, distinctUntilChanged, switchMap, shareReplay, take } from 'rxjs/operators';
import { PlaceData } from '../rs/rs.component';
import { Router } from '@angular/router';
import { UtilService } from '../util.service';

@Component({
  selector: 'app-rs-add',
  templateUrl: './rs-add.component.html',
  styleUrls: ['./rs-add.component.css']
})
export class RsAddComponent implements AfterViewInit {
  @ViewChild('pacInput', { static: false }) pacInput: ElementRef;
  @ViewChild('anchor', { static: false }) anchor: ElementRef;

  placeDetailTrigger$ = new Subject<string>();
  place$: Observable<google.maps.places.PlaceResult>;
  isPlaceSelected = false;

  constructor(
    private firestore: AngularFirestore,
    private userService: UserService,
    private utilService: UtilService,
    private mapService: MapService,
    private router: Router,
  ) { }

  ngAfterViewInit() {
    this.place$ = merge(
      this.placeDetailTrigger$,
      this.mapService
        .autocomplete$(this.pacInput.nativeElement, { componentRestrictions: { 'country': 'id' } })
        .pipe(map(place => place.place_id))
    ).pipe(
      distinctUntilChanged(),
      switchMap(async placeId => {
        this.isPlaceSelected = true;
        const snap = await this.firestore.collection('rs')
          .doc<PlaceData>(placeId)
          .get().pipe(take(1)).toPromise();
        if (!snap.exists) {
          const place = await this.userService.post<google.maps.places.PlaceResult>(
            `place_details/${placeId}`, {});
          this.router.navigate(['/rs-update', place.place_id]);
          return place;
        }
        const placeData = snap.data() as PlaceData;
        if (this.utilService.isRsInfoFilled(placeData)) {
          this.router.navigate(['/rs', placeData.place.place_id]);
        } else {
          this.router.navigate(['/rs-update', placeData.place.place_id]);
        }
        return placeData.place;
      }),
      shareReplay(1)
    );
  }

  scrollIntoView() {
    this.anchor.nativeElement.scrollIntoView({ behavior: "smooth", block: "end" });
  }
}
